package de.schubartulm.myschubart;

/**
 * AndroidApp created by Benedikt
 */

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.webkit.WebView;


public final class Global extends Application {

    public volatile static Activity ACTIVITY;
    public volatile static Context CONTEXT;
    public volatile static WebView WEBVIEW;

    public static String HTTP_URL = "http://192.168.0.25/";

    public static String HTML_INDEX = "index.html";
    public static String HTML_FILES_LIST= "files.txt";
}


