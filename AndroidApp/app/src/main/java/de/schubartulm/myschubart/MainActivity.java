package de.schubartulm.myschubart;

import android.app.Activity;
import android.content.pm.ApplicationInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class MainActivity extends Activity {

    public static String URL = "http://192.168.0.25";

    public WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (0 != (getApplicationInfo().flags & ApplicationInfo.FLAG_DEBUGGABLE)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                WebView.setWebContentsDebuggingEnabled(true);
            }
        }

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        webView = findViewById(R.id.webView);

        Global.WEBVIEW = webView;

        Global.CONTEXT = this;
        Global.ACTIVITY = this;

        if (LocalStorage.get("html_index").isEmpty()){
            LocalStorage.set("html_index", Global.HTML_INDEX);
        }

        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.canGoBackOrForward(2);
        webView.setVerticalScrollBarEnabled(false);
        webView.setHorizontalScrollBarEnabled(false);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            webView.getSettings().setMediaPlaybackRequiresUserGesture(false);
        }

        JSInterface jsInterface = new JSInterface();

        webView.addJavascriptInterface(jsInterface, "Android");

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                onUIThread(new Runnable() {
                    @Override
                    public void run() {

                    }
                });

                return true;
            }
            @Override
            public void onPageFinished(final WebView view, String url) {

                JSInterface.executeJS("alert('ok')", new DataCallback(){
                    @Override
                    public void call(String res) {

                    }
                });
                //view.loadUrl("javascript:(function() {Endless.setHtml('<html>'+document.getElementsByTagName('html')[0].innerHTML+'</html>');})();");
            }
        });

        loadWebViewContent();

        //Test - update local files with every app launch
        if(Info.networkAvailable()){
            FileManager.update();
            Toast.makeText(Global.ACTIVITY, "Downloading...", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(Global.ACTIVITY, "Error no internet connection", Toast.LENGTH_LONG).show();
        }
    }

    public void loadWebViewContent(){

        Log.e("WebViewContent", "(re)load");
        if(Info.networkAvailable()){
            Log.e("load ViewContent", Global.HTTP_URL);
            webView.clearCache(true);
            webView.loadUrl(Global.HTTP_URL);
        } else {
            webView.clearCache(true);
            Log.e("cached WebViewContent",LocalStorage.get("html_dir"));
            webView.loadUrl("file://"+LocalStorage.get("html_dir") + LocalStorage.get("html_index"));
        }
    }

    @Override
    public void onBackPressed(){
        try {
            loadWebViewContent();
            Toast.makeText(Global.ACTIVITY, "Update", Toast.LENGTH_SHORT).show();
        } catch (Exception e){e.printStackTrace();}
    }


    public static void onUIThread(Runnable runnable) {
        final Handler UIHandler = new Handler(Looper.getMainLooper());
        UIHandler.post(runnable);
    }

    public interface DataCallback{
        void call(String res);
    }
}
