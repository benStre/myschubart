package de.schubartulm.myschubart;

/**
 * AndroidApp created by Benedikt
 */

import android.app.AlertDialog;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.webkit.ValueCallback;

import static de.schubartulm.myschubart.Global.ACTIVITY;
import static de.schubartulm.myschubart.Info.getDeviceName;
import static de.schubartulm.myschubart.MainActivity.onUIThread;

public class JSInterface {

    //Storage
    @JavascriptInterface
    public String Storage_get(String key){
        String value=LocalStorage.get(key);
        Log.e("Storage value", value);
        return value;
    }

    @JavascriptInterface
    public void Storage_set(String key, String value){
        Log.e("Set value", key + " : " + value);
        LocalStorage.set(key, value);
    }

    @JavascriptInterface
    public String Storage_getAll(){
        String values = LocalStorage.getAll();
        Log.e("All values", values);
        return values;
    }

    @JavascriptInterface
    public String getDeviceInfo(){
        return getDeviceName();
    }
    @JavascriptInterface
    public void alert(String title, String message){
        new AlertDialog.Builder(ACTIVITY)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("OK", null)
                .setNegativeButton("Cancel", null)
                .show();
    }

//


    public static void callJS(final String function_name, final String param, final MainActivity.DataCallback callback){
        onUIThread(new Runnable(){
            @Override
            public void run() {
                Log.e("JS call", function_name);
                try {
                    Global.WEBVIEW.evaluateJavascript("(function() { \"use strict\";return "+function_name+"('"+param+"'); })();", new ValueCallback<String>() { //function_name+"(\""+params[0]+"\");"
                        @Override
                        public void onReceiveValue(String value) {
                            Log.w("JS call result", value);
                            try {
                                callback.call(value);
                            } catch (Exception ignored){}
                        }
                    });
                }catch (Exception e){e.printStackTrace();}
            }
        });

    }

    public static void executeJS( final String code, final MainActivity.DataCallback callback){
        onUIThread(new Runnable(){
            @Override
            public void run() {
                Log.e("JS execute", code);
                try {
                    Global.WEBVIEW.evaluateJavascript("(function() { \"use strict\";"+code+" })();", new ValueCallback<String>() {
                        @Override
                        public void onReceiveValue(String value) {
                            Log.w("JS execute result", value);
                            callback.call(value);
                        }
                    });
                }catch (Exception e){e.printStackTrace();}
            }
        });

    }


}
