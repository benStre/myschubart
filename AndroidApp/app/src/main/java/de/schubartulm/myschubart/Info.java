package de.schubartulm.myschubart;

/**
 * AndroidApp created by Benedikt
 */
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.util.Log;

import static de.schubartulm.myschubart.Global.CONTEXT;


public class Info {
    public static boolean networkAvailable(){
        ConnectivityManager connectivityManager
                = (ConnectivityManager) CONTEXT.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        boolean active = activeNetworkInfo != null && activeNetworkInfo.isConnected();
        if (active){
            Log.w("Info", "network available");
        }else {
            Log.e("Info", "network not available");
        }
        return active;
    }

    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return model;
        }
        manufacturer = manufacturer.toUpperCase().charAt(0)+manufacturer.substring(1,manufacturer.length());
        return manufacturer + " " + model;
    }
}