package de.schubartulm.myschubart;

import android.os.Environment;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;

import static de.schubartulm.myschubart.Global.HTML_FILES_LIST;


/**
 * MySchubart created by Benedikt
 */

public class FileManager {

    public static String getStorageDir(String dirName) {
        // Get the directory for the app's private pictures directory.
        File file = new File(Global.CONTEXT.getExternalFilesDir(
                Environment.DIRECTORY_DOCUMENTS), dirName);
        if (!file.mkdirs()) {
            Log.e("Files", "directory not/already created");
        }
        return file.getPath() +"/";
    }

    public static void writeFile(String filename, String content){
        File file = new File(LocalStorage.get("html_dir")+filename);
        try {
            if (!file.exists())
                file.createNewFile();
            BufferedWriter writer = new BufferedWriter(new FileWriter(file, false));

            writer.write(content);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static void saveDirectory(String[] list) {
        Log.w("FileManager Download", Arrays.toString(list));
        for (String file : list){
            new DownloadTask(Global.ACTIVITY, file, null);
        }

    }

    public static void update(){
        if(LocalStorage.get("html_dir").isEmpty()){
            String storageDir = FileManager.getStorageDir("html");
            Log.w("Storage Dir", storageDir);
            LocalStorage.set("html_dir", storageDir);
        }

        new DownloadTask(Global.ACTIVITY, HTML_FILES_LIST, new MainActivity.DataCallback() {
            @Override
            public void call(String res) {
                FileManager.saveDirectory(res.split("\n"));
            }
        });

    }
}
