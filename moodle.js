var TAG = "Moodle"

//════ MODULES══════════════════════════════════════╗
const puppeteer = require('puppeteer');
//══════════════════════════════════════════════════╝

var browser

class Moodle{
    
    constructor(data, c){
        this.init(data, c)   
    }
    // async "constructor"
    async init(data, c){
        _i(TAG, "+ new moodle instance")

        if(data.saved_login){
            await this.newInstance(CONFIG.URLS.MOODLE_HOME, data.moodle_cookies)
            console.log("loaded +cookies")
            c({valid:true})
        } else {
            await this.newInstance(CONFIG.URLS.MOODLE_LOGIN)
            
            await this.page.type( '#username', data.username )
            await this.page.type( '#password', data.password )
            await this.page.click( '#loginbtn' );
            
            await Promise.race([
                this.page.waitForSelector("span.error"),
                this.page.waitForNavigation()
            ])
            let url = await this.page.url()
            console.log(url)
            if(url==CONFIG.URLS.MOODLE_HOME){
                let cookies = await this.page.cookies()
                console.log(cookies)
                _s(TAG, "moodle login successful")
                c({valid:true, cookies:cookies})
            } else {
                _e(TAG, "moodle login error")
                c({valid:false})
            }
            
        
        }     
    }

    async clear(){
        await this.page.close()
    }
    

    async newInstance(url=CONFIG.URLS.MOODLE_HOME, cookies=false){
        this.page = await browser.newPage();
        if(cookies){
            await this.page.setCookie(...cookies);
        } else {
            await this.page._client.send('Network.clearBrowserCookies');
        }
        await this.page.goto(url);
    }

    static async launchHeadlessBrowser(ready){
        browser = await puppeteer.launch({headless: true});
        _s(TAG, "Headless browser launched successfully")
        ready()
    }
}


module.exports = Moodle
_s("Moodle", "initialized")