const VERSION = "0.1"


//════ MODULES══════════════════════════════════════╗
var express = require('express')
var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);
var colors = require('colors');
var uniqid = require('uniqid');
//══════════════════════════════════════════════════╝


//════ COLOR FORMATTING ════════════════════════════╗
var output_format = "console"
process.argv.forEach(function (val, index, array) {
  if(val=="--chrome") output_format = "chrome"
  if(val=="--console") output_format = "console"
});
console.clear()
console.log("-- Color formatting optimized for " + output_format+" --\n\n")
//══════════════════════════════════════════════════╝



//════ LOG ═════════════════════════════════════════╗
const DEF = [ "#ffffff", "#e5e5e5", "white" ]
const ERR = [ "#e5102f", "#c60320", "red"   ] 
const SUC = [ "#06ad36", "#058429", "green" ]
const INF = [ "#337fcc", "#2164a8", "cyan"  ]

global._ = function(tag, type, ...text){
   
   if (text.length>1){
    let first = text.shift()
    if(output_format=="chrome"){
      console.log(`%c[${tag}] %c${first}`, `color: ${type[0]}`, `color: ${type[1]}`, ...text)
    } else {
      console.log(`[${tag}]`[type[2]].bold, `${first}`[type[2]], ...text)
    }
   } else {
     if(output_format=="chrome"){
      console.log(`%c[${tag}] %c${text}`, `color: ${type[0]}`, `color: ${type[1]}`)
     } else {
      console.log(`[${tag}]`[type[2]].bold, `${text}`[type[2]])
     }
   }
  
}

global._e = function(tag, ...text){_(tag, ERR, ...text)}
global._d = function(tag, ...text){_(tag, DEF, ...text)}
global._s = function(tag, ...text){_(tag, SUC, ...text)}
global._i = function(tag, ...text){_(tag, INF, ...text)}

//══════════════════════════════════════════════════╝
_d(`═══ mySchubart SERVER v.${VERSION}═══`)


//════ CUSTOM MODULES ══════════════════════════════╗

var database = require("./database.js")()
var moodle = require("./moodle.js")
var user_session = require("./user_session.js")
//══════════════════════════════════════════════════╝


//════ CONSTANTS ═══════════════════════════════════╗

global.KEYS = {}
global.CONFIG = {}

function loadConfigAndKeys() {
    var fs = require("fs");
    var path = require('path');
    var config = fs.readFileSync(path.join(__dirname, "config.json"));
    CONFIG = JSON.parse(config);

    var keys = fs.readFileSync(path.join(__dirname, "keys.json"));
    KEYS = JSON.parse(config);
}

//══════════════════════════════════════════════════╝




//════ HTTP SERVER ═════════════════════════════════╗
loadConfigAndKeys()

_i("Config", "...", CONFIG)


moodle.launchHeadlessBrowser(()=>{
  startHttpServer()
})

function startHttpServer(){
  server.listen(80, ()=>_s("HTTP SERVER", "running..."));
  app.use(express.static('public',{
  extensions: ['html']
}))  
}

//══════════════════════════════════════════════════╝




//════ SOCKET ══════════════════════════════════════╗
io.on('connection', function (socket) {
  _i("SOCKET", "new socket connected")
  
  //socket.emit('message', { hello: 'world' });
  

  socket.on('sign_in', (data, c) => {
    SOCKET_getDataFromSessionId(data, c, socket)  // get user data + create new session
  });

  socket.on('sign_up', (data, c) => {
    SOCKET_createSession(data, c, socket)  // create new session with user data + add to database
  });

  socket.on('sign_out', (data, c) => { // sign out from session -> delete completely
    SOCKET_deleteSession(socket.session_id, c, socket)
  });

  socket.on('disconnect', () => { // disconnect event -> clear session
    SOCKET_clearSession(socket.session_id, ()=>{}, socket)
  });
  
  // debug - todo remove
  socket.on('_user_list', (data, c) => {
    c(user_session.getUserSessions())
  });

  // debug - todo remove
  socket.on('user_message', (data, c) => {
    console.log(data, socket.session_id)
  });

  socket.on('notify', (data, c) => {
    Notification(data.username, data.text)
  });

});
//══════════════════════════════════════════════════╝




//════ Notifications ═══════════════════════════════╗

global.Notification = (username, message)=>{
  _i("Notification", username, message)  
}

//══════════════════════════════════════════════════╝




//════ SOCKET functions ════════════════════════════╗


// remove session completely
async function SOCKET_deleteSession(session_id, c, socket){
    if(session_id){
      await database.removeUserSession(session_id) // clear from database
      await user_session.removeSession(session_id) // remove from session array + clear moodinst
      c({valid:true})
    }
    else c({valid:false, error:"session not valid"})
}


// just remove session from current running sessions
async function SOCKET_clearSession(session_id, c, socket){
    if(session_id){
      await user_session.removeSession(session_id) // remove from session array + clear moodinst
      c({valid:true})
    }
    else c({valid:false, error:"session not valid"})
}


// [sign in data, callback]
async function SOCKET_getDataFromSessionId(data, c, socket){
  try {
      //_i("SOCKET", "user sign in data: ", data);

      // get user data for this session
      let user_data = await database.getUserData(data.session_id)
      // valid user session?
      if(user_data){
        _s("SOCKET", `${user_data.username} signed in with session_id ${data.session_id}`)
        user_data.session_id = data.session_id
        SOCKET_createSession(user_data, c, socket)
      } else {
        _e("SOCKET", `sign in error`)
        c({valid:false})
      }

  } catch(err){_e("SOCKET", "sign_in", err.message);c({valid:"error"})}
}


// [sign up data, callback]
async function SOCKET_createSession(data, c, socket){
  try {

      let moodle_cookies = await database.getMoodleCookies(data.username)
      
      // already logged in with moodle account
      if(moodle_cookies){
        _i("SOCKET", "existingMoodleSession")
        existingMoodleSession(data, moodle_cookies, c, socket)
      } else {       
        _i("SOCKET", "newMoodleSession")
        // first moodle login
        newMoodleSession(data, c, socket)
      }

      } catch(err){_e("SOCKET", "sign_up", err.message);c({valid:"error"})}
}



async function newMoodleSession(data, c, socket){
  // try to login on moodle
  var moodinst = new moodle({saved_login:false, username:data.username, password:data.password},
      async (result)=>{
        // valid user? 
        if(result.valid){
          console.log(result)
          if(!data.session_id){
            data.session_id = createSessionId()
            _i("SOCKET", "new session_id: ", data.session_id)
              // create new user and save session_id/moodle cookies or add current session_id
              let valid_user = await database.updateOrNewUser(data.session_id, data, result.cookies);
              if(!valid_user){        
                 // remove moodle instance
                 await moodinst.clear()
                 delete moodinst 

                 c({valid:false, error:"invalid password"})
                 return;
              } 
          } else { 
             _i("SOCKET", "using session_id ", data.session_id)

          }

          //create new Session
          user_session.addSession(data.session_id, data, moodinst)
          //add session_id to socket
          socket.session_id = data.session_id
          c({valid:true, session_id:data.session_id})
         
        } else {
          // remove moodle instance
          await moodinst.clear()
          delete moodinst 
          c({valid:false, error:"no valid moodle login data"})
        }

      })
}


async function existingMoodleSession(data, moodle_cookies, c, socket){
  var moodinst = new moodle({saved_login:true, moodle_cookies:moodle_cookies}, 
  async (result)=>{
      if(result.valid){
        if(!data.session_id){
          data.session_id = createSessionId()
          _i("SOCKET", "new session_id: ", data.session_id)
          // create new user and save session_id/moodle cookies or add current session_id
          let valid_user = await database.updateOrNewUser(data.session_id, data, moodle_cookies);
          if(!valid_user){
             c({valid:false, error:"invalid password"})
             // remove moodle instance
             await moodinst.clear()
             delete moodinst 

             return;
          }          
        } else {
           _i("SOCKET", "using session_id ", data.session_id)
        }

        //create new Session
        user_session.addSession(data.session_id, data, moodinst)
        //add session_id to socket
        socket.session_id = data.session_id
        c({valid:true, session_id:data.session_id})
      } else {
         c({valid:false, error:"moodle session expired"})
        //todo handle moodle session expired!!!
      }

  })

}




function createSessionId(){
  return uniqid()
}

//══════════════════════════════════════════════════╝


