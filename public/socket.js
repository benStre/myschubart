class Socket {

    constructor(){
        this.TAG = "SOCKET"
        

        if(navigator.onLine && !NO_SERVER_CONNECTION){        
            _i(this.TAG, "ONLINE")
        } else {
            _e(this.TAG, "OFFLINE")
        }
        
        this.init()

        _s(this.TAG, "initialized")
    }

    init(){
        
        if(navigator.onLine && !NO_SERVER_CONNECTION){
            // initialize socket connection
            this.socket = io.connect(SOCKET_SERVER_URL);   
            
            // define events
            this.on_message = data => _(TAG, "message:", data)
            this.socket.on('message', data=>this.on_message(data));         
        } else {
            this.socket = new OfflineSocket()
        }

    }

    async socket_emit(tag, data){
        return new Promise(resolve => {
            this.socket.emit(tag, data, (data)=>{
            resolve(data);
            });         
        });                      
    }


    async sign_up(data) { // {user: "xyz", password:"123"}
        return await this.socket_emit("sign_up", data)
    }

    async sign_in(data) { // {session_id: "asd8ui32jekwla"}
        return await this.socket_emit("sign_in", data)
    }

    async sign_out(data) {
        return await this.socket_emit("sign_out", data)
    }

    async user_message(data) { // {text: "hello"}
        return await this.socket_emit("user_message", data)
    }
    
    // debug functions - todo remove
    async _user_list(data) { 
        return await this.socket_emit("_user_list", data)
    }    

}



class OfflineSocket{

    constructor(){

    }

    emit(tag, data, callback){
        _i("OfflineSocket", "emit", tag, data, callback)

        if(tag=="sign_up"){
            callback({valid:false, reason:"offline"})
        }

        else if(tag=="sign_in"){
            if(data.session_id){
                callback({valid:true, session_id:data.session_id})
            } else {
                callback({valid:false})
            }
        }

        else if(tag=="sign_out"){
            callback({valid:true})
        } 


        else  callback({valid:false})

        // ....
    }

}