const VERSION = "0.1"

//════ LOG ═════════════════════════════════════════╗
const DEF = [ "#ffffff", "#e5e5e5", "white" ]
const ERR = [ "#e5102f", "#c60320", "red"   ] 
const SUC = [ "#06ad36", "#058429", "green" ]
const INF = [ "#337fcc", "#2164a8", "cyan"  ]

var _ = function(tag, type, ...text){
    
   if (text.length>1){
    let first = text.shift()
    console.log(`%c[${tag}] %c${first}`, `color: ${type[0]}`, `color: ${type[1]}`, ...text)    
   } else {
    console.log(`%c[${tag}] %c${text}`, `color: ${type[0]}`, `color: ${type[1]}`)
   }
  
}

var _e = function(tag, ...text){_(tag, ERR, ...text)}
var _d = function(tag, ...text){_(tag, DEF, ...text)}
var _s = function(tag, ...text){_(tag, SUC, ...text)}
var _i = function(tag, ...text){_(tag, INF, ...text)}

//══════════════════════════════════════════════════╝



//════ CONSTANTS ═══════════════════════════════════╗
const SOCKET_URL = "http://192.168.0.25/socket.io/socket.io.js"
const SOCKET_SERVER_URL = "http://192.168.0.25/"

const COLORS = {grey: "#555", lightgrey:"#888", blue: "#3995ec", green: "#009100", red:"#e31642"}
//══════════════════════════════════════════════════╝




//════ VARIABLES ═══════════════════════════════════╗
var SOCKET, INTERFACE, UI

var socket_io_js = false // socket.io.js loaded?
var NO_SERVER_CONNECTION = false // cannot connect to server - same as with no internet connection
//══════════════════════════════════════════════════╝


window.onload = INIT

//════ Do everything here: ═════════════════════════╗
async function INIT() {
    _d(`═══ mySchubart APP v.${VERSION}═══`)

    //════ init other modules ═══╗
    INTERFACE = new Interface()
    UI = new UserInterface()
    //═══════════════════════════╝
  
    //load socket.io if online
    await loadSocketIo()
      
    // launch screen
    UI.launchScreen_load()
  
    // online/offline events
    window.addEventListener('online', ONLINE);
    window.addEventListener('offline', OFFLINE);  

    SOCKET = new Socket() // init socket module

    await sleep(1000)
    
    // check if already signed in
    if (INTERFACE.GET_session_id()){
        _i("INIT", "session saved")
        let valid = await SIGN_IN()
        if (valid){
            UI.homeScreen()  // show home screen
        }
    } else {
        _i("INIT", "no session saved, new login")

        let valid = await UI.launchScreen_login() //async (animation)
        while (!valid){
            console.log("try launchScreen_login")
            valid = await UI.launchScreen_login(true)
        }
        UI.homeScreen()  // show home screen

    }
}


async function OFFLINE(){
    _s("Network", "OFFLINE")    
    UI.dialog("Keine Internetverbindung")
    SOCKET.init() //reset socket - offline
}

async function ONLINE(){
    _s("Network", "ONLINE")
    let socket_loaded = await loadSocketIo() // load socket.io.js
    
    UI.dialog("Internetverbindung wiederhergestellt", "blue")
    SOCKET.init() //reset socket - online 
}


function loadSocketIo(){
    return new Promise(async resolve => {
        if(navigator.onLine){     
            _i("SocketIo.js", "loading ...")
            let socket_loaded = await loadScript(SOCKET_URL)
            if(socket_loaded){
                resolve()
            } else { // no server connection, but internet
                _e("server connection error")
                NO_SERVER_CONNECTION = true   
                resolve()
            }
        } else {
            _e("SocketIo.js", "cannot load, offline") 
            NO_SERVER_CONNECTION = true
            resolve()
        }   
    }) 
}

function loadScript(src) {
    return new Promise(resolve => {
      var s,r,t;
      r = false;
      s = document.createElement('script');
      s.type = 'text/javascript';
      s.src = src;
      s.onload = s.onreadystatechange = function() {
        //console.log( this.readyState ); //uncomment this line to see which ready states are called.
        if ( !r && (!this.readyState || this.readyState == 'complete') )
        {
          r = true;
          socket_io_js = true
          resolve(true);
        }
      };

      s.onerror = function() {
          resolve(false)
      }

      t = document.getElementsByTagName('script')[0];
      t.parentNode.insertBefore(s, t);        
    })

}
//══════════════════════════════════════════════════╝


//════ Login functions ═════════════════════════════╗

async function SIGN_UP(username, password){
    return new Promise(async resolve => {
        _i("sign up", username + ", " + password)
        result = await SOCKET.sign_up({username: username, password:password, device_info:INTERFACE.getDeviceInfo()})    
        _i("sign_up result", ":", result)    
        if(result.valid){
            _s("sign up", "successful")
            INTERFACE.SET_session_id(result.session_id)
            resolve(true)
        } else {
            _e("sign up", "not valid")
            resolve(false)
        }

    })

}

async function SIGN_IN(){
    return new Promise(async resolve => {
        result = await SOCKET.sign_in({session_id: INTERFACE.GET_session_id()})
        console.log(result)
        if(result.valid){
            _s("sign in", "successful")        
            resolve(true)
        } else {
            _e("sign in", "not valid")
            resolve(false)
        }
    })
}

//══════════════════════════════════════════════════╝


// functions
function sleep(time){
     return new Promise((resolve, reject)=> {
       setTimeout(resolve, time)  
     })
}

