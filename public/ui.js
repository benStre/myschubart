class UserInterface {

    constructor(){
        this.TAG = "UI"

        this.screens = ["launch_screen", "home_screen"]
        this.sections = ["top_menu", "bottom_menu"]

        this.tabs = ["home", "courses", "schedule", "chat", "files"]

        this.key_events = {}
        
        document.body.onkeypress = (key)=>{ // handle key press events 
            let code = key.key.toLowerCase()
            _i("KeyHandler", "event for " + code)     
            if(this.key_events[code]){               
                let keep_event = this.key_events[code]()      
                if(!keep_event) this.resetKeyEvent(key)     
            }
        };

        this.resetScreens()
        this.resetSections()

        this.setupClickEvents()
        _s(this.TAG, "initialized")
    }

    setupClickEvents(){
        // bottom menu
        $(`#bottom_menu .column`).on("click touchstart", (e)=>{
            let tab = e.currentTarget.dataset.tab

            if(this.tabs.includes(tab)){
                _i("UI", `open tab "${tab}"`)
                // reset other icons
                $("#bottom_menu path").css("color", COLORS["lightgrey"])
                // set new element color to blue
                $(e.currentTarget).find("path").css("color", COLORS["blue"])

                this.tabContent(tab)                
            } else {
                _e("UI", `tab "${tab}" not available`)
            }

        })

        $(`#search_icon_div`).on("click touchstart", (e)=>{
            this.expandSearchBar()           
        })
        $(`#search_input_div`).focusout((e)=>{
            this.collapseSearchBar()           
        })
        $(`#account_menu_div`).on("click touchstart", (e)=>{
            this.openSettings()           
        })
    }

    expandSearchBar(){
        _i("Search Bar", "expand")
        $(`#search_icon_div`).css("display", "none")
        $(`#search_input_div`).css("display", "inline")
        $(`#search_input`).css("opacity", "0")
        $(`#search_input`).focus()
        $('#search_input').animate({opacity:1,duration:'300ms'});
    }

    collapseSearchBar(){
        _i("Search Bar", "collapse")
        $('#search_input').animate({opacity:0,duration:'300ms'}, 300, ()=>{
            $(`#search_icon_div`).css("display", "inline")        
            $(`#search_input_div`).css("display", "none")              
        });
    }
    

    openSettings(){
        _i("UI", "open settings")
        $(`#settings`).css("display", "inline")
        $(`body`).css("height", "100%")
        $(`body`).css("overflow", "hidden")
    }

    tabContent(tab){
        $('#home_screen').html(tab)
    }

    setKeyEvent(key, func){
        this.key_events[key] = func
    }
    resetKeyEvent(key){
        delete this.key_events[key]
    }

    resetScreens(){
        for (let screen of this.screens){
            $(`#${screen}`).css('display', 'none');
        }
    }

    resetSections(){
        for (let section of this.sections){
            $(`#${section}`).css('display', 'none');
        }
    }
        
    // display screens:
    launchScreen(){
        this.resetScreens()
        $('#launch_screen').css('display', 'initial');
    }

    // display section:
    showSection(id){
        $(`#${id}`).css('display', 'block');
    }

    hideSection(id){
        $(`#${id}`).css('display', 'none');
    }
        
    async launchScreen_login(wrong=false){

        this.launchScreen()
        return new Promise(resolve => {
            $('#launch_screen_content').html(`<h5 class="subtitle is-5 mys_title">mySchubart</h5>
                <div class="field">
                  <p class="control has-icons-left has-icons-right">
                    <input class="input" type="email" id="input_username" placeholder="Benutzername">
                    <span class="icon is-small is-left">
                      <i class="fas fa-user"></i>
                    </span>
                    <span id="righticon_username" class="icon is-small is-right">
                    </span>
                  </p>
                </div>
                <div class="field">
                  <p class="control has-icons-left has-icons-right">
                    <input class="input" type="password" id="input_password" placeholder="Passwort">
                    <span class="icon is-small is-left">
                      <i class="fas fa-lock"></i>
                    </span>
                    <span id="righticon_password" class="icon is-small is-right">
                    </span>
                  </p>
                </div>
                <a class="button is-info" id="btn_login">&nbsp;&nbsp;OK&nbsp;&nbsp;</a>`) 
           


            if(wrong){ // last login data wasn't valid
                this.username_icon(false)
                this.password_icon(false)
            }
            

            $('#input_username, #input_password').on('input', ()=> { 
                let username = $('#input_username').val()
                let password = $('#input_password').val()
                this.checkUsernameAndPassword(username, password)
            });



            var enter = async ()=>{
                this.resetKeyEvent("enter")

                let username = $('#input_username').val()
                let password = $('#input_password').val()
                if(this.checkUsernameAndPassword(username, password)){
                    this.launchScreen_load()
                    let valid = await SIGN_UP(username , password)
                    resolve(valid)   
                                
                } else {
                    this.setKeyEvent("enter", ()=>{ // key event for enter
                      enter();return false  
                    })                      
                }
                
            }
            
            this.setKeyEvent("enter", ()=>{ // key event for enter
              enter();return false  
            })         
            $('#btn_login').click(enter) // set button click listener -> login


            $('#launch_screen_content').animate({
                height: "+=250"
              }, 500, function() {
                  $('#input_username').focus()
              }); 
            })   
    }

    username_icon(right){
        $("#righticon_username").html(`<i class="fas fa-${right?"check":"times"}"></i>`)
    }

    password_icon(right){
        $("#righticon_password").html(`<i class="fas fa-${right?"check":"times"}"></i>`)
    }

    checkUsernameAndPassword(username, password){
        let u,p

        if(username.length>1){     
          this.username_icon(true)    
          u = true  
        } else this.username_icon(false)

        if(password.length>0){
            this.password_icon(true)
          p = true
        } else this.password_icon(false)
        
        return (u&&p)
    }



    launchScreen_load(){
        this.launchScreen()
        $('#launch_screen_content').html("").animate({
                height: "0"
              }, 500, function() {
             }); 
    }

    
    homeScreen(){
        this.resetScreens()
        $('#home_screen').css('display', 'initial');   
        this.showSection("top_menu")
        this.showSection("bottom_menu")
    }


    dialog(text="", color_name="grey", time=3000){
       let color = COLORS[color_name]
       if(this.notification_timeout) clearTimeout(this.notification_timeout)
       $(".notification").remove()
       $("html").append(`<div class="is-info notification" style="background-color:${color}">
        ${text}
        </div>`)
       $(".notification").css("top", "-100px").animate({
                top: "+=100"
              }, 500, function() {}); 
       this.notification_timeout = setTimeout(()=>{
           $(".notification").remove()
       }, time)
    }

}



