class Interface {

    constructor(){
        this.TAG = "INTERFACE"

        if(window.Android){
            _s(this.TAG, "initialized for Android native app")
        } else {
            _s(this.TAG, "initialized for web app")
        }
        
    }


    getDeviceInfo(){
        // todo change for native app
        if(window.Android){
            return Android.getDeviceInfo()
        } else {
            var os = [
                { name: 'Windows Phone', value: 'Windows Phone', version: 'OS' },
                { name: 'Windows', value: 'Win', version: 'NT' },
                { name: 'iPhone', value: 'iPhone', version: 'OS' },
                { name: 'iPad', value: 'iPad', version: 'OS' },
                { name: 'Kindle', value: 'Silk', version: 'Silk' },
                { name: 'Android', value: 'Android', version: 'Android' },
                { name: 'PlayBook', value: 'PlayBook', version: 'OS' },
                { name: 'BlackBerry', value: 'BlackBerry', version: '/' },
                { name: 'Macintosh', value: 'Mac', version: 'OS X' },
                { name: 'Linux', value: 'Linux', version: 'rv' },
                { name: 'Palm', value: 'Palm', version: 'PalmOS' }
            ]

            var browser = [
                { name: 'Chrome', value: 'Chrome', version: 'Chrome' },
                { name: 'Firefox', value: 'Firefox', version: 'Firefox' },
                { name: 'Safari', value: 'Safari', version: 'Version' },
                { name: 'Internet Explorer', value: 'MSIE', version: 'MSIE' },
                { name: 'Opera', value: 'Opera', version: 'Opera' },
                { name: 'BlackBerry', value: 'CLDC', version: 'CLDC' },
                { name: 'Mozilla', value: 'Mozilla', version: 'Mozilla' }
            ]
            var header = [
                navigator.platform,
                navigator.userAgent,
                navigator.appVersion,
                navigator.vendor,
                window.opera
            ];

            var agent = header.join(' ');
            var os = this.tools_matchItem(agent, os).name
            var browser = this.tools_matchItem(agent, browser).name
            return `${os} (${browser})`            
        }

    }
    
    // Variables / storage:

    Storage_get(key){
        if(window.Android){
            return Android.Storage_get(key)
        } else {
            return localStorage.getItem(key)
        }
        
    }

    Storage_set(key, value){
        if(window.Android){
            return Android.Storage_set(key, value)
        } else {
            return localStorage.setItem(key, value)
        }
    }
    

    // get session_id
    GET_session_id(){
        return this.Storage_get("session_id") || false
    }

    // set session_id
    SET_session_id(value){
        return this.Storage_set("session_id", value)
    }


    tools_matchItem(string, data) {
        var i = 0,
            j = 0,
            html = '',
            regex,
            regexv,
            match,
            matches,
            version;

        for (i = 0; i < data.length; i += 1) {
            regex = new RegExp(data[i].value, 'i');
            match = regex.test(string);
            if (match) {
                regexv = new RegExp(data[i].version + '[- /:;]([\d._]+)', 'i');
                matches = string.match(regexv);
                version = '';
                if (matches) { if (matches[1]) { matches = matches[1]; } }
                if (matches) {
                    matches = matches.split(/[._]+/);
                    for (j = 0; j < matches.length; j += 1) {
                        if (j === 0) {
                            version += matches[j] + '.';
                        } else {
                            version += matches[j];
                        }
                    }
                } else {
                    version = '0';
                }
                return {
                    name: data[i].name,
                    version: parseFloat(version)
                };
            }
        }
        return { name: 'unknown', version: 0 };
    }

}





