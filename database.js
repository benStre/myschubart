const TAG = "Database"

const DATABASE_user = "root"
const DATABASE_password = "sikretKiforMaieskuel!"
const DATABASE_host = "localhost"
const DATABASE_port = "3308"
const DATABASE_database_name = "myschubart"


//════ MODULES══════════════════════════════════════╗
const mysql = require('mysql');
//══════════════════════════════════════════════════╝

module.exports = ()=>{
    return new Database()
}

//════ Database ════════════════════════════════════╗

class Database{

    constructor(){

        this.connect()
        
        _s(TAG, "initialized")
    }

    connect(){
       // create database connection
       this.con = mysql.createConnection({
          host: DATABASE_host,
          user: DATABASE_user,
          password: DATABASE_password,
          port: DATABASE_port,
          database: DATABASE_database_name
        });

        this.con.connect((err)=> {
          if (err) throw err;
          _s(TAG, `connected to database '${DATABASE_database_name}'`);
        });
    }
    
    // return data from users table for a session_id
    async getUserData(session_id){
        if(!session_id) return false

        return new Promise((resolve, reject)=> {
            this.con.query("SELECT * FROM `users` WHERE JSON_CONTAINS_PATH(`sessions`, 'one', ?)", [`$."${session_id}"`], (err, result, fields)=> {
                if (err){reject(err);return}     
                result = result[0]
                // parse JSON
                if (result){
                    result.sessions = JSON.parse(result.sessions)
                    result.device_info = result.sessions[session_id].device_info
                    resolve(result)                     
                } else {
                    resolve(false)
                }
            })
        })
    }
    
    // create new user or add session_id
    async updateOrNewUser(session_id, data, cookies){
        if(!session_id) return false

        return new Promise((resolve, reject)=> {

            // get user data -> check password etc.
            this.con.query("SELECT * FROM `users` WHERE username=?", [data.username], (err, result, fields)=> {
                if (err){reject(err);return;}  
                console.log(result)

                //console.log("updateOrNewUser", data.username) 
                let exists = Boolean(result[0])
                                 
                
                // user already in database
                if(exists){
                    console.log(data.username, "already in database")

                    if(result[0].username==data.username && result[0].password==data.password){ // check password (unencrypted!!!)
                        _s("Database", "password valid")
                         console.log("UPDATE `users` SET `sessions`=JSON_INSERT(`sessions`, ?, CAST('"+this.createSessionItem(data.device_info)+"' AS JSON)) WHERE `username`=?")
                         // add new session to database
                         this.con.query("UPDATE `users` SET `sessions`=JSON_INSERT(`sessions`, ?, CAST('"+this.createSessionItem(data.device_info)+"' AS JSON)) WHERE `username`=?", [`$."${session_id}"`, data.username], (err, result, fields)=> {
                            if (err){reject(err);return;}     
                            resolve(true)
                         });
                        
                    } else {
                        _e("Database", "password invalid")      
                        resolve(false)
                    }

                } else {
                     console.log(`add ${data.username} to database`)
                    // insert new user
                    this.con.query("INSERT INTO `users` (`username`, `password`, `sessions`, `moodle_cookies`) VALUES(?, ?, ?, ?)", [data.username, data.password, this.createSessionObject(session_id, data.device_info), JSON.stringify(cookies)], (err, result, fields)=> {
                        if (err){reject(err);return;}     

                        //console.log(result)
                        resolve(true)
                        
                        return;

                        ////////////////////
                       
                        result = result[0]
                        // parse JSON
                        if (result){
                            result.sessions = JSON.parse(result.sessions)
                            resolve(result)                     
                        } else {
                            resolve(false)
                        }
                    })                    
                }

            })


        })
    }

    // remove session from database
    async removeUserSession(session_id){
        if(!session_id) return false

        return new Promise((resolve, reject)=> {
            this.con.query("UPDATE `users` SET `sessions`=JSON_REMOVE(`sessions`, ?)", [`$."${session_id}"`], (err, result, fields)=> {
                if (err){reject(err);return}     
                resolve(true)
            })
        })
    }
    
    
    // get moodle cookies if set
    async getMoodleCookies(username){
        if(!username) return false

        return new Promise((resolve, reject)=> {
            this.con.query("SELECT `moodle_cookies` FROM `users` WHERE username=?", [username], (err, result, fields)=> {
                if (err){reject(err);return}     
                
                result = (result[0] && result[0].moodle_cookies) ? JSON.parse(result[0].moodle_cookies) : false
                if(result=="null") result=false
                //console.log("cookies", result)
                // parse JSON
                if (result){
                    resolve(result)                     
                } else {
                    resolve(false)
                }
            })
        })
    }


    createSessionItem(device_info){
        return `{"device_info":"${mysql.escape(device_info).replace(/\'/g, "")}"}`
    }
    
    createSessionObject(session_id, device_info){
        return `{"${mysql.escape(session_id).replace(/\'/g, "")}":${this.createSessionItem(device_info)}}`
    }

}