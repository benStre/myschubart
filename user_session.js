var user_sessions = {}

class UserSession {

    constructor(session_id, user_data, moodinst){
        this.TAG = "UserSession #"+user_data.username

        this.session_id = session_id
        this.user_data = user_data
        this.moodinst = moodinst
        
        _i(this.TAG, "created")

    }


    static addSession(session_id, user_data, moodinst){
        user_sessions[session_id] = new UserSession(session_id, user_data, moodinst)
    }

    static async removeSession(session_id){
        await user_sessions[session_id].moodinst.clear()
        delete user_sessions[session_id]
    }

    static getById(session_id) { user_sessions[session.session_id] }

    static getUserSessions() { 
        let data = []
        for (let session_id of Object.keys(user_sessions)){
            data.push(user_sessions[session_id].user_data)   
        }
        return data
    }
}

module.exports = UserSession
_s("UserSession", "initialized")
